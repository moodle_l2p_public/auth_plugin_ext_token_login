<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The Moodle External Token Authentication Plugin
 *
 * @package    auth_ext_token_login
 * @author     Marco Schlicht
 * @copyright  2017 Marco Schlicht PIT RWTH Aachen
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @link       https://git.rwth-aachen.de/moodle_l2p_public/auth_plugin_ext_token_login
 *
 * auth_ext_token_login
 * Copyright (C) 2017 Marco Schlicht PIT RWTH Aachen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

defined('MOODLE_INTERNAL') || die();

if ($ADMIN->fulltree) {
    $settings->add(new admin_setting_heading(
        'auth_ext_token_login/pluginname', 'Admin Settings',
        get_string('auth_ext_token_logindescription', 'auth_ext_token_login')
    ));
    $authplugin = get_auth_plugin('ext_token_login');
    $settings->add(new admin_setting_configtext(
        'ext_token_login_auth_url',
        get_string('settings_auth_url_visible_name', 'auth_ext_token_login'),
        get_string('settings_auth_url_description', 'auth_ext_token_login'),
        'http://your-oauth-application.de/login.php'
    ));
    $settings->add(new admin_setting_configtext(
        'ext_token_login_auth_service_id',
        get_string('settings_auth_service_id_visible_name', 'auth_ext_token_login'),
        get_string('settings_auth_service_id_description', 'auth_ext_token_login'),
        '1234567890'
    ));
    $settings->add(new admin_setting_configtext(
        'ext_token_login_auth_service_scope',
        get_string('settings_auth_service_scope_visible_name', 'auth_ext_token_login'),
        get_string('settings_auth_service_scope_description', 'auth_ext_token_login'),
        'abcdefgh'
    ));
    $settings->add(new admin_setting_configcheckbox(
        'ext_token_login_enable_loginpage',
        get_string('settings_enable_loginpage_visible_name', 'auth_ext_token_login'),
        get_string('settings_enable_loginpage_description', 'auth_ext_token_login'),
        true
    ));
    $settings->add(new admin_setting_configtext(
        'ext_token_login_auth_username_prefix',
        get_string('settings_auth_username_prefix_visible_name', 'auth_ext_token_login'),
        get_string('settings_auth_username_prefix_description', 'auth_ext_token_login'),
        ''
    ));
    $settings->add(new admin_setting_configcheckbox(
        'ext_token_login_enable_webservice',
        get_string('settings_enable_webservice_visible_name', 'auth_ext_token_login'),
        get_string('settings_enable_webservice_description', 'auth_ext_token_login'),
        true
    ));
    $settings->add(new admin_setting_configtext(
        'ext_token_login_webservice_dummy_user',
        get_string('settings_webservice_dummy_user_visible_name', 'auth_ext_token_login'),
        get_string('settings_webservice_dummy_user_description', 'auth_ext_token_login'),
        'ext_token_login_user'
    ));
    $settings->add(new admin_setting_configcheckbox(
        'ext_token_login_enable_password_login',
        get_string('settings_enable_password_login_visible_name', 'auth_ext_token_login'),
        get_string('settings_enable_password_login_description', 'auth_ext_token_login'),
        false
    ));
}

// vim: ts=4 sw=4 et
