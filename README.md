# External Token Login Authentication Plugin

The External Token Login Authentication Plugin is an authentication plugin for Moodle.
It allows users to **login with a token, that gets verified against an external service** for example oauth.
You can login on the normal login page by just adding the POST or GET parameter 'token' to the login url.
It's also possible to login users as a webservice. The users don't need to be a ext\_token\_login user for this.

## Synopsis
### Normal Login
Login with GET or POST parameter 'token=externaltoken':

```
https://yourmoodle.de/login/index.php?token=externaltoken
```

### Webservice login
Login as a webservice and receive a webservice token with GET or POST parameters:

*Warning:* Site admins are not able to create webservice tokens, so it's not possible to log them in with this plugin over the webservice login.

```
https://yourmoodle.de/login/token.php?username=ext_token_login_user&password=&token=externaltoken&service=yourservice
```

## Requirements

### <a name="requireddependencies"></a> Dependencies
- libcurl>=7.10.5 (to use curl in php)
- an external authentication service
	- for example OAuth
	- it gets called with the GET parameters service\_id, service\_scope, access\_token
	- it has to return the associated username as a json string in this format:
		- '{"status": "ok", "userId": $username}'

### <a name="requiredwebservice"></a> Moodle Webservice
Only required if you want to use the webservice login.

1. Create a webservice, long and short name could be for example "webservice\_w"

_Optionally: If you want all users to login with this plugin_
1. Uncheck the `Authorised users` option
2. Select `No required capability`


## <a name="installation"></a>Installation
1. Install it in Moodle to /auth/ext\_token\_login
	- `cd $moodle/auth/ && git clone git@git.rwth-aachen.de:moodle_l2p_public/auth_plugin_ext_token_login.git ext_token_login`
2. Go to the plugin settings (side\_administration-\>plugins-\>authentication-\>ext\_token\_login\_authentication)
3. Configure your url, OAuth Service-Id and OAuth Service-Scope
4. Enable ext\_token\_login\_authentication in side\_administration-\>plugins-\>authentication-\>manage\_authentication

### Installation Normal Login
Only required if you want to use the normal login. This is if you go to the moodle login page and append a token to the url to login like a normal user but with a token instead of a username and password.

1. Activate "*Enable Loginpage Login*" in plugin settings
2. Now you can use normal login with your token

	```
	https://yourmoodle.de/login/index.php?token=externaltoken
	```

### <a name="installationwebservice"></a>Installation Webservices
Only required if you want to use the webservice login.

1. Enable webservices. Read the docs how to do this and make sure it works, so that there are no complications.
2. Create a webservice dummy user (eg. 'ext\_token\_login\_user') with the authentication method 'ext\_token\_login'.
	- It's enough to just insert the user with an SQL statement:

		```sql
		INSERT INTO `moodle`.`mdl_user` (`username`, `auth`) VALUE ("ext_token_login_user", "ext_token_login");
		```

	*This is needed, because the webservice login don't have a (pre-authentication)hook that is called on every active plugin like it is on the normal login. The webservice login only activates the ext\_token\_login plugin for valid ext\_token\_login users, so we need this bypass.*

3. Save your webservice dummy user in the plugin settings and *Enable Webservice Login*

4. Only if you have users with ext\_token\_login as authentication method (except the dummy webservice user), enable "*Enable Password Login*", to give these users the possibility to login to a webservice with there normal password. If this is not activated, they can only login with a token.

### <a name="externaltokenloginservice"></a> External Token Login Service
**Required.** You need an external service that validates your tokens. You can use (not anymore, but may be updated soon) [this Example](https://git.rwth-aachen.de/moodle_l2p_public/ext_token_login_service_example) or the RWTH Aachen OAuth.

1. Goto plugin settings and insert all required data for the oauth login.


## How to use it:
### Normal Login
Login with GET or POST parameter 'token=externaltoken':
```
https://yourmoodle.de/login/index.php?token=externaltoken
```

### Webservices
Now every not ext\_token\_login user can login to the webservices by two ways,
the normal **(old) way** that was already possible before by sending his username and password
```
https://yourmoodle.de/login/token.php?username=yourname&password=geheim&service=yourservice
```

And the second **(new) way** is to login with the ext\_token\_login user and a token.
Username is now the created dummy user 'ext\_token\_login\_user' and the password is empty.
You have to add a third parameter 'token' which contains your external token.
```
https://yourmoodle.de/login/token.php?username=ext_token_login_user&password=&token=externaltoken&service=yourservice
```
Durinig the login the ext\_token\_login user get changed with the external user and the external user webservice token is returned.


## RWTH Api
Settings for the RWTH Api.

### Requirements
Check the requirements:

1. [Dependencies](#requireddependencies)

### Installation
1. Install it in Moodle to /auth/ext\_token\_login
	- `cd $moodle/auth/ && git clone git@git.rwth-aachen.de:moodle_l2p_public/auth_plugin_ext_token_login.git ext_token_login`
2. Run the [l2p install script](https://git.rwth-aachen.de/moodle_l2p/l2p_scripts)
