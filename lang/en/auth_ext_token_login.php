<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The Moodle External Token Authentication Plugin
 *
 * @package    auth_ext_token_login
 * @author     Marco Schlicht
 * @copyright  2017 Marco Schlicht PIT RWTH Aachen
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @link       https://git.rwth-aachen.de/moodle_l2p_public/auth_plugin_ext_token_login
 *
 * auth_ext_token_login
 * Copyright (C) 2017 Marco Schlicht PIT RWTH Aachen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

$string['auth_ext_token_logindescription'] = 'Users can sign in with a token that get checked against an external service';
$string['pluginname'] = 'ext_token_login authentication';
$string['login_greeting'] = 'Hello {$a}. You signed in with a Token.';
// Settings.
$string['settings_auth_url_visible_name'] = 'Authentification-Url';
$string['settings_auth_url_description'] = 'The ext_token_login plugin authentificates the tokens agaist this url.';

$string['settings_auth_service_id_visible_name'] = 'OAuth Service-Id';
$string['settings_auth_service_id_description'] = 'The id of this application.';

$string['settings_auth_service_scope_visible_name'] = 'OAuth Service-Scope';
$string['settings_auth_service_scope_description'] = 'The scope of this application.';

$string['settings_enable_loginpage_visible_name'] = 'Enable Loginpage Login';
$string['settings_enable_loginpage_description'] = 'If enabled, you can login on the loginpage with a token.';

$string['settings_auth_username_prefix_visible_name'] = 'Username Prefix';
$string['settings_auth_username_prefix_description'] = 'The Prefix would be added before the username.';

$string['settings_enable_webservice_visible_name'] = 'Enable Webservice Login';
$string['settings_enable_webservice_description'] = 'If enabled, you can login into a webservice with a token.';

$string['settings_webservice_dummy_user_visible_name'] = 'Webservice Dummy User';
$string['settings_webservice_dummy_user_description'] = 'The dummy user, that you use to login into webservices. Is only necessary, when Webservice Login is enabled. If you fill in this field, only this user can be used to authenticate others with tokens.';

$string['settings_enable_password_login_visible_name'] = 'Enable Password Login';
$string['settings_enable_password_login_description'] = 'If enabled, ext_token_login users can login on the loginpage with a password.<br>Should be disabled, if you only have the dummy user with auth="ext_token_login"';
// vim: ts=4 sw=4 et
