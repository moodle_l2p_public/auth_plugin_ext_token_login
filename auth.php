<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The Moodle External Token Authentication Plugin
 *
 * @package    auth_ext_token_login
 * @author     Marco Schlicht
 * @copyright  2017 Marco Schlicht PIT RWTH Aachen
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @link       https://git.rwth-aachen.de/moodle_l2p_public/auth_plugin_ext_token_login
 *
 * auth_ext_token_login
 * Copyright (C) 2017 Marco Schlicht PIT RWTH Aachen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/authlib.php');

// These vars must be stored in GLOBALS, because in the webservice login.
// There is more than one instance of this class, so we can't use $this.
// Reference to the user that is loaded at the end.
$exttokenloginuserref;
// Username of the external user.
$exttokenloginextuser;

// Echo debug messages.
$debug;

/**
 * The Moodle External Token Authentication Plugin
 *
 * @package    auth_ext_token_login
 * @author     Marco Schlicht
 * @copyright  2017 Marco Schlicht PIT RWTH Aachen
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @link       https://git.rwth-aachen.de/moodle_l2p_public/auth_plugin_ext_token_login
 **/
class auth_plugin_ext_token_login extends auth_plugin_base
{

    /**
     * Constructor
     *
     * @return none
     **/
    public function __construct() {
        global $debug;
        // Set debug to true, to enable debugging messages.
        $debug = false;
        $this->authtype = 'ext_token_login';
    }

    /**
     * Wenn das passwort anders ist, als das gespeicherte, dann wird es nach erfolgreichem login geupdated.
     * true ändert das passwort
     * false ändert das passwort auf "not cached" (wtf?)
     *
     * @return bool
     **/
    public function prevent_local_passwords() {
        return false;
    }

    /**
     * False because the plugin authenticates against an external service.
     **/
    public function is_internal() {
        return false;
    }

    /**
     * False because user can't change password.
     **/
    public function can_change_password() {
        return false;
    }

    /**
     * Modifies user in external database. Is called when the user needs to be updated.
     *
     * @param stdClass $olduser Userobject before modifications
     * @param stdClass $newuser Userobject new modified userobject
     *
     * @return true if update was successful or ignored; false if an error appeared
     */
    public function user_update($olduser, $newuser) {
        return false;
    }

    /**
     * Wird am Anfang des webservice logins ausgeführt.
     * Wird nicht ausgeführt bei webseiten login
     * Wird benutzt um die user referenz zu speichern
     *
     * @param stdClass $user User
     **/
    public function pre_user_login_hook(&$user) {
        global $exttokenloginextuser, $CFG;
        if ($CFG->ext_token_login_enable_webservice) {
            // Save user reference, old and new user in global variables,
            // this is used to prevent the password update.
            $globals["exttokenloginuserref"] = &$user;
            $token = optional_param('token', '', PARAM_TEXT);
            if (isset($token)) {
                // Token login:
                // The new user gets loged in to let moodle check his security settings.
                $user = $this->external_login($token);
                // Save external user.
                $exttokenloginextuser = $user;
            }
        }
    }

    /**
     * Wird als erstes ausgeführt, um den login zu unterbrechen.
     * Für den normalen login über die loginpage, nicht bei webservice
     */
    public function loginpage_hook() {
        global $SESSION, $CFG;
        if ($CFG->ext_token_login_enable_loginpage) {
            $token = optional_param('token', '', PARAM_TEXT);
            if (empty($token)) {
                // Missing parameters or its just the login page.
                return;
            } else {
                // Get user from external service.
                $user = $this->external_login($token);
                if (empty($user)) {
                    // Invalid token.
                    return;
                }
                // Login.
                authenticate_user_login($user->username, null);
                complete_user_login($user);
                // Redirect.
                if ( isset($SESSION->wantsurl) and (strpos($SESSION->wantsurl, $CFG->wwwroot) === 0) ) {
                    // The url requested befor the login.
                    $urltogo = $SESSION->wantsurl;
                    unset($SESSION->wantsurl);
                } else {
                    // No wantsurl stored or external - go to homepage.
                    $urltogo = new moodle_url("/my");
                    unset($SESSION->wantsurl);
                }
                redirect($urltogo);
            }
        }
    }

    /**
     * on normal login: if false, use the next auth plugin and don't authenticate the user with this.
     * username and password works, if auth is = manual and username is saved in database
     *
     * webservice login: if true the user gets authorized, if false he gets not autorized
     * wird bei webservices vor update_internal_user_password aufgerufen.
     * wir setzen hier die referenz zum user object (am ende der funktion) zurück auf den
     * alten (ext_token_login) user um das zurücksetzen des passwords (beim webservice login) zu verhindern.
     *
     * @param string $username Username
     * @param string $password Password
     */
    public function user_login($username, $password) {
        global $exttokenloginextuser, $exttokenloginuserref, $DB, $CFG;
        if ($CFG->ext_token_login_enable_webservice) {
            $token = optional_param('token', '', PARAM_TEXT);
            if (isset($token)) {
                // Token login.
                // Username und password sind egal.
                $dummyuser = $DB->get_record('user', array('username' => $username, 'mnethostid' => $CFG->mnet_localhost_id));
                $extuser = $exttokenloginextuser;
                /* dummy user must exist and have the right auth */
                /* ext user must exist */
                /* accept dummy user, only if it is equals to the settings user or if the settings user is empty */
                if (
                    ( !empty($dummyuser) && ($dummyuser->auth == 'ext_token_login') ) &&
                    ( !empty($extuser) ) &&
                    (
                        empty($CFG->ext_token_login_webservice_dummy_user) ||
                        $username === $CFG->ext_token_login_webservice_dummy_user
                    )
                ) {
                    // Valid login:
                    // Now the old user must be loged in, otherwise the user password would be deleted.
                    // The external user will be loged in later in the is_synchronised_with_external() method.
                    $exttokenloginuserref = $dummyuser;
                    return true;
                } else {
                    // Debugging ausgaben.
                    if (!empty($dummyuser)) {
                        $this->debug_echo("missing parameter username");
                    } else if (
                        (
                            !( empty($CFG->ext_token_login_webservice_dummy_user) ) ||
                            !( $username === $CFG->ext_token_login_webservice_dummy_user )
                        ) &&
                        ($dummyuser->auth == 'ext_token_login')
                    ) {
                        $this->debug_echo("this dummy user is not allowed to login other users with token");
                    } else if ( empty($extuser) ) {
                        $this->debug_echo("empty external user (no user asociated with token)");
                    }
                    // Invalid login.
                    return false;
                }
            } else if ($CFG->ext_token_login_enable_password_login) {
                // Diesen teil müssen wir hinzufügen, wenn user auth=ext_token_login haben. wenn wir das nicht machen,
                // dann kann sich ein user nicht mit username und passwort einloggen, sondern nur über den token.
                global $CFG, $DB, $USER;
                if (!$user = $DB->get_record('user', array('username' => $username, 'mnethostid' => $CFG->mnet_localhost_id))) {
                    return false;
                }
                if (!validate_internal_user_password($user, $password)) {
                    return false;
                }
                if ($password === 'changeme') {
                    // Force the change - this is deprecated and it makes sense only for manual auth,
                    // because most other plugins can not change password easily or
                    // passwords are always specified by users.
                    set_user_preference('auth_forcepasswordchange', true, $user->id);
                }
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }


    /**
     * Wird nach update_internal_user_password (nach user_login) aufgerufen.
     * Set the user back to the newuser to make the authentication work
     **/
    public function is_synchronised_with_external() {
        global $exttokenloginextuser, $exttokenloginuserref;
        $exttokenloginuserref = $exttokenloginextuser;
    }

    /**
     * Lädt den user, welcher mit dem token verknüpft ist und returnt ihn
     *
     * @param string $token External token
     **/
    public function external_login($token) {
        global $DB, $CFG;
        // Login to external client and load external userinformation.
        if (empty($CFG->ext_token_login_auth_url)) {
            $this->debug_echo("no url");
            return;
        }
        if (empty($CFG->ext_token_login_auth_service_id)) {
            $this->debug_echo("no service id");
            return;
        }
        if (empty($CFG->ext_token_login_auth_service_scope)) {
            $this->debug_echo("no service scope");
            return;
        }
        $url = $CFG->ext_token_login_auth_url;
        $serviceid = $CFG->ext_token_login_auth_service_id;
        $servicescope = $CFG->ext_token_login_auth_service_scope;
        $accesstoken = preg_replace('/[^-a-zA-Z0-9_]/', '', $token);
        $url = "$url?service_id=$serviceid&service_scope=$servicescope&access_token=$accesstoken";
        $this->debug_echo($url);
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $output = curl_exec($curl);
        $this->debug_echo($output);
        $response = json_decode($output);
        if (empty($response)) {
            $this->debug_echo("empty response");
            return;
        }
        if (empty($response->status)) {
            $this->debug_echo("invalid oauth reply");
            return;
        }
        if ($response->status != "ok") {
            $this->debug_echo("oauth login failed");
            return;
        }
        // Get username.
        if (empty($response->userId)) {
            $this->debug_echo("oauth returned no uid");
            return;
        }
        $username = "$CFG->ext_token_login_auth_username_prefix";
        $username .= $response->userId;
        // Get all user information.
        $dbuser = $DB->get_record('user', array('username' => $username, 'mnethostid' => $CFG->mnet_localhost_id));
        if (empty($dbuser)) {
            $this->debug_echo("this user does not exist");
            // Create user.
            // Prefil information.
            // This shouldn't happen.
            return;
        }
        return $dbuser;
    }

    /**
     * Echo a string, but only if global var $debug is true
     *
     * @param string $string String that should be printed
     **/
    private function debug_echo($string) {
        global $debug;
        if ($debug) {
            echo "$string; ";
        }
    }

}
// vim: ts=4 sw=4 et
